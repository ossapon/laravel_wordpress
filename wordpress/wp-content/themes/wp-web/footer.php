<section id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="main_footer_area white-text p-b-3">
                <div class="col-md-3">
                    <div class="single_f_widget p-t-3 wow fadeInUp">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="" />
                        <div class="single_f_widget_text">
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
                                The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                            <div class="socail_f_widget">
                                <a href="#!" ><i class="fa fa-facebook"></i></a>
                                <a href="#!" ><i class="fa fa-google-plus"></i></a>
                                <a href="#!" ><i class="fa fa-twitter"></i></a>
                                <a href="#!" ><i class="fa fa-vimeo"></i></a>
                                <a href="#!" ><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_f_widget m-t-3 wow fadeInUp">
                        <h4 class="text-lowercase">Some features</h4>
                        <div class="single_f_widget_text f_reatures">
                            <ul>
                                <li><i class="fa fa-check"></i>Lorem ipsum dolor sit amet</li>
                                <li><i class="fa fa-check"></i>Aliquam tincidunt cons ectetuer</li>
                                <li><i class="fa fa-check"></i>Vestibulum auctor dapibus con</li>
                                <li><i class="fa fa-check"></i>Lorem ipsum dolor sit amet auctor dapibus</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_f_widget m-t-3 wow fadeInUp">
                        <h4 class="text-lowercase">Tags</h4>
                        <div class="single_f_widget_text f_tags">
                            <a href="#!">corporate</a>
                            <a href="#!">agency</a>
                            <a href="#!">portfolio</a>
                            <a href="#!">blog</a>
                            <a href="#!">elegant</a>
                            <a href="#!">professional</a>
                            <a href="#!">business</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single_f_widget m-t-3 wow fadeInUp">
                        <h4 class="text-lowercase">Flicker Posts</h4>
                        <div class="single_f_widget_text f_flicker">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flipcker1.jpg" alt="" />
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flipcker2.jpg" alt="" />
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flipcker3.jpg" alt="" />
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flipcker4.jpg" alt="" />
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flipcker3.jpg" alt="" />
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flipcker2.jpg" alt="" />
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flipcker4.jpg" alt="" />
                            <img src="<?php echo get_template_directory_uri(); ?>/img/flipcker1.jpg" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main_coppyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <div class="copyright_text m-t-2 text-xs-center">
                        <p class="wow zoomIn" data-wow-duration="1s">Made with <i class="fa fa-heart"></i> by <a target="_black" href="http://bootstrapthemes.co"> Bootstrap Themes</a> 2016. All Rights Reserved</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="socail_coppyright text-sm-right m-t-2 text-xs-center wow zoomIn">
                        <a href="#!"><i class="fa fa-facebook"></i></a>
                        <a href="#!"><i class="fa fa-twitter"></i></a>
                        <a href="#!"><i class="fa fa-google-plus"></i></a>
                        <a href="#!"><i class="fa fa-rss"></i></a>
                        <a href="#!"><i class="fa fa-vimeo"></i></a>
                        <a href="#!"><i class="fa fa-pinterest"></i></a>
                        <a href="#!"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>







<!-- /Start your project here-->


<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-2.2.3.min.js"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/tether.min.js"></script>


<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/mdb.min.js"></script>

<!-- Wow js -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>

<!-- Mixitup js -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mixitup.min.js"></script>

<!-- Magnific-popup js -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.magnific-popup.js"></script>

<!-- accordion js -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/accordion.js"></script>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/materialize.js"></script>

<script>
    $(".button-collapse").sideNav();
</script>

<!-- wow js active -->
<script type="text/javascript">
    new WOW().init();
</script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/plugins.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>

 <?php wp_footer();?>
</body>

</html>