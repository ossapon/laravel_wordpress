<?php get_header(); ?>


<div id="home" class="slider">
    <ul class="slides">
        <li>
            <img src="<?php echo get_template_directory_uri(); ?>/img/homebenner.jpg"> <!-- random image -->
            <div class="caption center-align">
                <div class="single_home">
                    <h1><?php echo get_bloginfo('name');?></h1>
                    <p><?php echo get_bloginfo('description');?></p>
                    <button type="button" class="btn btn-danger m-t-3 waves-effect waves-red">See More</button>
                </div>
            </div>
        </li>
        <li>
            <img src="<?php echo get_template_directory_uri(); ?>/img/homebenner.jpg"> <!-- random image -->
            <div class="caption center-align">
                <div class="single_home">
                    <h1><?php echo get_bloginfo('name');?></h1>
                    <p><?php echo get_bloginfo('description');?></p>
                    <button type="button" class="btn btn-danger m-t-3 waves-effect waves-red">See More</button>
                </div>
            </div>
        </li>
        <li>
            <img src="<?php echo get_template_directory_uri(); ?>/img/homebenner.jpg"> <!-- random image -->
            <div class="caption center-align">
                <div class="single_home">
                    <h1><?php echo get_bloginfo('name');?></h1>
                    <p><?php echo get_bloginfo('description');?></p>
                    <button type="button" class="btn btn-danger m-t-3 waves-effect waves-red">See More</button>
                </div>
            </div>
        </li>
        <li>
            <img src="<?php echo get_template_directory_uri(); ?>/img/homebenner.jpg"> <!-- random image -->
            <div class="caption center-align">
                <div class="single_home">
                    <h1><?php echo get_bloginfo('name');?></h1>
                    <p><?php echo get_bloginfo('description');?></p>
                    <button type="button" class="btn btn-danger m-t-3 waves-effect waves-red">See More</button>
                </div>
            </div>
        </li>
    </ul>
</div>







<section id="about" class="about">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="main_about_area">
                    <div class="head_title center m-y-3 wow fadeInUp">
                        <h2><?php echo get_cat_name(3);?></h2>
                        <p><?php echo category_description(3);?></p>
                    </div>

                    <div class="main_about_content">
                        <div class="row">
                            <div class="col-md-6">

                                <div class="main_accordion wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s" data-wow-offset="0">

                                    <div class="single_accordion">
                                        <button class="accordion"><?php echo get_cat_name(4);?></button>
                                        <div class="panel">
                                            <p><?php echo category_description(4);?></p>
                                        </div>
                                    </div>

                                    <div class="single_accordion">
                                        <button class="accordion"><?php echo get_cat_name(6);?></button>
                                        <div class="panel">
                                            <p><?php echo category_description(6);?></p>
                                        </div>
                                    </div>

                                    <div class="single_accordion">
                                        <button class="accordion"><?php echo get_cat_name(7);?></button>
                                        <div class="panel">
                                            <p><?php echo category_description(7);?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="single_about about_progress">
                                    <div class="skill wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s" data-wow-offset="0">
                                        <!-- progress start -->
                                        <div class="progress">
                                            <div class="lead">Web Development 95%</div>
                                            <div class="progress-bar wow fadeInLeft" data-progress="95%" style="width: 95%;" data-wow-duration="1.5s" data-wow-delay="1.2s"></div>
                                        </div>
                                        <!-- progress end -->
                                        <!-- progress start -->
                                        <div class="progress">
                                            <div class="lead">App Development 90%</div>
                                            <div class="progress-bar wow fadeInLeft" data-progress="90%" style="width: 90%;" data-wow-duration="1.5s" data-wow-delay="1.2s"></div>
                                        </div>
                                        <!-- progress end -->
                                        <!-- progress start -->
                                        <div class="progress">
                                            <div class="lead">Software Development 85%</div>
                                            <div class="progress-bar wow fadeInLeft" data-progress="85%" style="width: 85%;" data-wow-duration="1.5s" data-wow-delay="1.2s"></div>
                                        </div>
                                        <!-- progress end -->
                                        <!-- progress start -->
                                        <div class="progress">
                                            <div class="lead">Graphics Design 80%</div>
                                            <div class="progress-bar wow fadeInLeft" data-progress="80%" style="width: 80%;" data-wow-duration="1.5s" data-wow-delay="1.2s"></div>
                                        </div>
                                        <!-- progress end -->
                                        <!-- progress start -->
                                        <div class="progress">
                                            <div class="lead">Marketing 70%</div>
                                            <div class="progress-bar wow fadeInLeft" data-progress="70%" style="width: 70%;" data-wow-duration="1.5s" data-wow-delay="1.2s"></div>
                                        </div>
                                        <!-- PROGRESS END -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <br />
    <br />
    <br />
    <hr />
</section><!-- End of About Section-->



<section id="service" class="service">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="main_service_area">
                    <div class="head_title center m-y-3 wow fadeInUp">
                        <h2><?php echo get_cat_name(8);?></h2>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="jumbotron single_service  wow fadeInLeft">
                                <div class="service_icon center">
                                    <i class="fa fa-cog m-b-3"></i>
                                </div>
                                <div class="s_service_text text-sm-center text-xs-center">
                                    <h4><?php echo get_cat_name(9);?></h4>
                                    <p><?php echo category_description(9);?></p>
                                </div>

                                <div class="service_btn center">
                                    <a href="#!" class="btn btn-danger waves-effect waves-red">See  More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="jumbotron single_service wow fadeInUp">
                                <div class="service_icon center">
                                    <i class="fa fa-pied-piper m-b-3"></i>
                                </div>
                                <div class="s_service_text text-sm-center text-xs-center">
                                    <h4><?php echo get_cat_name(10);?></h4>
                                    <p><?php echo category_description(10);?></p>
                                </div>

                                <div class="service_btn center">
                                    <a href="#!" class="btn btn-danger waves-effect waves-red">See  More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="jumbotron single_service wow fadeInRight">
                                <div class="service_icon center">
                                    <i class="fa fa-plug m-b-3"></i>
                                </div>
                                <div class="s_service_text text-sm-center  text-xs-center">
                                    <h4><?php echo get_cat_name(11);?></h4>
                                    <p><?php echo category_description(11);?></p>
                                </div>

                                <div class="service_btn center">
                                    <a href="#!" class="btn btn-danger waves-effect waves-red">See  More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
</section> <!-- End of service section -->



<section id="joinus" class="joinus">
    <div class="main_joinus_area m-y-3" style="background-color: darkgrey;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main_joinus_content center white-text wow fadeInUp">
                        <i class="fa fa-user-plus m-b-1"></i>
                        <h2 class="text-uppercase m-b-3"><?php echo get_cat_name(12);?></h2>
                        <p><?php echo category_description(12);?></p>

                        <a href="#!" class="btn btn-danger waves-effect waves-red">Join with us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
</section> <!-- End of JoinUs section -->




<section id="offer" class="offer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main_offer_area">
                    <div class="head_title center m-y-3  wow fadeInUp">
                        <h2><?php echo get_cat_name(13);?></h2>
                    </div>
                    <div class="main_offer_content m-b-3  wow fadeInUp">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="single_offer m-t-3">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="single_offer_icon">
                                                <i class="fa fa-cloud"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="single_offer_text">
                                                <h3><?php echo get_cat_name(14);?></h3>
                                                <p><?php echo category_description(14);?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="single_offer m-t-3">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="single_offer_icon">
                                                <i class="fa fa-cart-arrow-down"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="single_offer_text">
                                                <h3><?php echo get_cat_name(15);?></h3>
                                                <p><?php echo category_description(15);?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="single_offer m-t-3">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="single_offer_icon">
                                                <i class="fa fa-line-chart"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="single_offer_text">
                                                <h3><?php echo get_cat_name(16);?></h3>
                                                <p><?php echo category_description(16);?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="single_offer m-t-3">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="single_offer_icon">
                                                <i class="fa fa-shopping-basket"></i>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="single_offer_text">
                                                <h3><?php echo get_cat_name(17);?></h3>
                                                <p><?php echo category_description(17);?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
</section><!-- End of Offer Section -->

<section id="client" class="client">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="main_client_area">
                    <div class="head_title center m-y-3 wow fadeInUp">
                        <h2><?php echo get_cat_name(18);?></h2>
                    </div>
                    <div class="main_client_content m-b-3">

                        <div class="carousel carousel-slider">
                            <div class="carousel-item" >
                                <div class="single_client_area">
                                    <div class="single_client">
                                        <div class="row">

                                            <?php if (have_posts()) : query_posts('p=42');
                                                while (have_posts()) : the_post(); ?>

                                            <div class="col-md-4">
                                                <div class="single_c_img center">
                                                    <?php if ( has_post_thumbnail() ) :?>
                                                        <a class="img-circle" href="<?php the_permalink();?>" title="<?php the_title_attribute();?>">
                                                            <?php the_post_thumbnail() ;?>
                                                        </a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="single_c_text text-md-left text-xs-center">
                                                    <span class="text-uppercase m-b-1" ><?php echo get_post(get_post_thumbnail_id())->post_content; ?></span>
                                                    <p><?php the_content(); ?></p>
                                                </div>
                                            </div>
                                                <?php endwhile; endif; ?>
                                        </div>
                                    </div>
                                    <div class="single_client">
                                        <div class="row">
                                            <?php if (have_posts()) : query_posts('p=44');
                                            while (have_posts()) : the_post(); ?>

                                            <div class="col-md-4">
                                                <div class="single_c_img center">
                                                    <?php if ( has_post_thumbnail() ) :?>
                                                        <a class="img-circle" href="<?php the_permalink();?>" title="<?php the_title_attribute();?>">
                                                            <?php the_post_thumbnail() ;?>
                                                        </a>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="single_c_text text-md-left text-xs-center">
<!--                                                    <h3 class="img-circle">--><?php //the_post_thumbnail();?><!--</h3>-->
                                                    <span class="text-uppercase m-b-1" >
                                                        <?php echo get_post(get_post_thumbnail_id())->post_content; ?></span>
                                                    <p><?php the_content(); ?></p>
                                                </div>
                                            </div>
                                            <?php endwhile; endif; wp_reset_query(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- End of carouser item -->
                        </div><!-- End of carouser slider -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
</section><!-- End of client Section -->


<section id="counter" class="counter">

    <div class="main_counter_area m-y-3">
        <div class="overlay p-y-3">
            <div class="container">
                <div class="row">
                    <div class="main_counter_content center white-text wow fadeInUp">
                        <div class="col-md-3">
                            <div class="single_counter p-y-2 m-t-1">
                                <i class="fa fa-heart m-b-1"></i>
                                <h2 class="statistic-counter">100</h2>
                                <p>Love Our Team</p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_counter p-y-2 m-t-1">
                                <i class="fa fa-check m-b-1"></i>
                                <h2 class="statistic-counter">400</h2>
                                <p>Check Our</p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_counter p-y-2 m-t-1">
                                <i class="fa fa-refresh m-b-1"></i>
                                <h2 class="statistic-counter">312</h2>
                                <p>repeat client</p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="single_counter p-y-2 m-t-1">
                                <i class="fa fa-beer m-b-1"></i>
                                <h2 class="statistic-counter">480</h2>
                                <p>Pizzas Ordered</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
</section><!-- End of counter Section -->



<section id="team" class="team">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="main_team_area m-y-3">
                    <div class="head_title center  wow fadeInUp">
                        <h2><?php echo get_cat_name(21);?></h2>
                        <p><?php echo category_description(21);?></p>
                    </div>


                    <div class="main_team_content center">
                        <div class="row">
                            <?php if (have_posts()) : query_posts('p=51');
                                while (have_posts()) : the_post(); ?>
                            <div class="col-md-3">
                                <div class="single_team white-text m-t-2 wow zoomIn">
                                    <?php the_post_thumbnail() ;?>
                                    <div class="single_team_overlay">
                                        <?php if ( has_post_thumbnail() ) :?>
                                            <a class="img-circle" href="<?php the_permalink();?>" title="<?php the_title_attribute();?>"></a>
                                        <?php endif; ?>
                                        <p><?php the_content(); ?></p>
                                        <div class="team_socail">
                                            <a href="#!"><i class="fa fa-facebook"></i></a>
                                            <a href="#!"><i class="fa fa-twitter"></i></a>
                                            <a href="#!"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#!"><i class="fa fa-dribbble"></i></a>
                                            <a href="#!"><i class="fa fa-behance"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; endif; ?>
                            <?php if (have_posts()) : query_posts('p=48');
                                while (have_posts()) : the_post(); ?>
                                    <div class="col-md-3">
                                        <div class="single_team white-text m-t-2 wow zoomIn">
                                            <?php the_post_thumbnail() ;?>
                                            <div class="single_team_overlay">
                                                <?php if ( has_post_thumbnail() ) :?>
                                                    <a class="img-circle" href="<?php the_permalink();?>" title="<?php the_title_attribute();?>"></a>
                                                <?php endif; ?>
                                                <p><?php the_content(); ?></p>
                                                <div class="team_socail">
                                                    <a href="#!"><i class="fa fa-facebook"></i></a>
                                                    <a href="#!"><i class="fa fa-twitter"></i></a>
                                                    <a href="#!"><i class="fa fa-pinterest-p"></i></a>
                                                    <a href="#!"><i class="fa fa-dribbble"></i></a>
                                                    <a href="#!"><i class="fa fa-behance"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; endif; ?>
                            <?php if (have_posts()) : query_posts('p=54');
                                while (have_posts()) : the_post(); ?>
                                    <div class="col-md-3">
                                        <div class="single_team white-text m-t-2 wow zoomIn">
                                            <?php the_post_thumbnail() ;?>
                                            <div class="single_team_overlay">
                                                <?php if ( has_post_thumbnail() ) :?>
                                                    <a class="img-circle" href="<?php the_permalink();?>" title="<?php the_title_attribute();?>"></a>
                                                <?php endif; ?>
                                                <p><?php the_content(); ?></p>
                                                <div class="team_socail">
                                                    <a href="#!"><i class="fa fa-facebook"></i></a>
                                                    <a href="#!"><i class="fa fa-twitter"></i></a>
                                                    <a href="#!"><i class="fa fa-pinterest-p"></i></a>
                                                    <a href="#!"><i class="fa fa-dribbble"></i></a>
                                                    <a href="#!"><i class="fa fa-behance"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; endif; ?>
                            <?php if (have_posts()) : query_posts('p=57');
                            while (have_posts()) : the_post(); ?>
                            <div class="col-md-3">
                                <div class="single_team white-text m-t-2 wow zoomIn">
                                    <?php the_post_thumbnail() ;?>
                                    <div class="single_team_overlay">
                                        <?php if ( has_post_thumbnail() ) :?>
                                            <a class="img-circle" href="<?php the_permalink();?>" title="<?php the_title_attribute();?>"></a>
                                        <?php endif; ?>
                                        <p><?php the_content(); ?></p>
                                        <div class="team_socail">
                                            <a href="#!"><i class="fa fa-facebook"></i></a>
                                            <a href="#!"><i class="fa fa-twitter"></i></a>
                                            <a href="#!"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#!"><i class="fa fa-dribbble"></i></a>
                                            <a href="#!"><i class="fa fa-behance"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; endif; ?><!-- End of col-sm-3 -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
</section><!-- End of Team section -->


<section id="portfolio" class="portfolio">
    <div class="container">
        <div class="row">
            <div class="main_portfolio_area m-y-3">
                <div class="head_title center wow fadeInUp">
                    <h2><?php echo get_cat_name(23);?></h2>
                    <p><?php echo category_description(23);?></p>
                </div>

                <div class="main_portfolio_content center wow fadeInUp">
                    <div class="main_mix_menu m-y-2">
                        <ul class="text-uppercase">
                            <li class="filter" data-filter="all">All</li>
                            <li class="filter" data-filter=".cat1">Photography</li>
                            <li class="filter" data-filter=".cat2">Graphic</li>
                            <li class="filter" data-filter=".cat3">Print</li>
                            <li class="filter" data-filter=".cat4">Web</li>
                        </ul>
                    </div>

                    <div id="mixcontent" class="mixcontent  wow zoomIn">






                        <div class="col-md-4 mix cat4 cat2">
                            <div class="single_mixi_portfolio center">
                                <div class="single_portfolio_img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/pf1.jpg" alt="" />
                                    <div class="mixi_portfolio_overlay">
                                        <a href="#!"><i class="fa fa-search"></i></a>
                                        <a href="<?php echo get_template_directory_uri(); ?>/img/pf1.jpg" class="gallery-img"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                                <div class="single_portfolio_text">
                                    <h3><?php echo get_cat_name(24);?></h3>
                                    <p><?php echo category_description(24);?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mix cat4 cat1">
                            <div class="single_mixi_portfolio center">
                                <div class="single_portfolio_img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/pf2.jpg" alt="" />
                                    <div class="mixi_portfolio_overlay">
                                        <a href="#!"><i class="fa fa-search"></i></a>
                                        <a href="<?php echo get_template_directory_uri(); ?>/img/pf2.jpg" class="gallery-img"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                                <div class="single_portfolio_text">
                                    <h3><?php echo get_cat_name(24);?></h3>
                                    <p><?php echo category_description(24);?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mix cat2 cat4">
                            <div class="single_mixi_portfolio center">
                                <div class="single_portfolio_img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/pf3.jpg" alt="" />
                                    <div class="mixi_portfolio_overlay">
                                        <a href="#!"><i class="fa fa-search"></i></a>
                                        <a href="<?php echo get_template_directory_uri(); ?>/img/pf3.jpg" class="gallery-img"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                                <div class="single_portfolio_text">
                                    <h3><?php echo get_cat_name(24);?></h3>
                                    <p><?php echo category_description(24);?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mix cat2 cat3">
                            <div class="single_mixi_portfolio center">
                                <div class="single_portfolio_img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/pf4.jpg" alt="" />
                                    <div class="mixi_portfolio_overlay">
                                        <a href="#!"><i class="fa fa-search"></i></a>
                                        <a href="<?php echo get_template_directory_uri(); ?>/img/pf4.jpg" class="gallery-img"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                                <div class="single_portfolio_text">
                                    <h3><?php echo get_cat_name(24);?></h3>
                                    <p><?php echo category_description(24);?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mix cat3 cat4">
                            <div class="single_mixi_portfolio center">
                                <div class="single_portfolio_img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/pf5.jpg" alt="" />
                                    <div class="mixi_portfolio_overlay">
                                        <a href="#!"><i class="fa fa-search"></i></a>
                                        <a href="<?php echo get_template_directory_uri(); ?>/img/pf5.jpg" class="gallery-img"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                                <div class="single_portfolio_text">
                                    <h3><?php echo get_cat_name(24);?></h3>
                                    <p><?php echo category_description(24);?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mix cat1 cat3">
                            <div class="single_mixi_portfolio center">
                                <div class="single_portfolio_img">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/pf6.jpg" alt="" />
                                    <div class="mixi_portfolio_overlay">
                                        <a href="#!"><i class="fa fa-search"></i></a>
                                        <a href="<?php echo get_template_directory_uri(); ?>/img/pf6.jpg" class="gallery-img"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                                <div class="single_portfolio_text">
                                    <h3><?php echo get_cat_name(24);?></h3>
                                    <p><?php echo category_description(24);?></p>
                                </div>
                            </div>
                        </div><!-- End of col-md-4 -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
</section><!-- End of Portfolio Section -->



<section id="works" class="works">
    <div class="container">
        <div class="row">
            <div class="main_works_area center m-y-3">
                <div class="head_title center  wow fadeInUp">
                    <h2><?php echo get_cat_name(23);?></h2>
                </div>

                <div class="main_works_content p-y-3">
                    <div class="col-md-3">
                        <div class="single_works wow zoomIn">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/works1.png" alt="" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="single_works wow zoomIn">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/works2.png" alt="" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="single_works wow zoomIn">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/works3.png" alt="" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="single_works wow zoomIn">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/works4.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
</section><!-- End of works Section -->


<section id="newsletter" class="newsletter">
    <div class="container">
        <div class="row">
            <div class="main_newsletter white-text p-y-2 m-t-3">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="single_newsletter left m-t-1 text-md-left text-sm-center text-xs-center wow zoomIn">
                        <p>Subscribe to our newsletter to get every Notify</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="single_newsletter text-md-right text-sm-center wow zoomIn">
                        <div class="md-form input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                        <button class="btn waves-effect waves-red" type="button"><i class="fa fa-send"></i></button>
                                    </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- End of Newsletter section -->

<?php get_footer();

