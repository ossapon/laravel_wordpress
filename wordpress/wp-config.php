<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '111111');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'VZ _:=JCEu,)Yz!Vur%m|hR79#P+3y*&o-eUst[H(~4cvc7>i=5^&L7l&H+@Ij^i');
define('SECURE_AUTH_KEY',  'e11ig-Ox~yu2ahBN=^!wy/9;GgeL>M!Sqv7qD~W2oLd(yDJ)]rc?ZH`0o+vn$!,L');
define('LOGGED_IN_KEY',    ' <bF]<^6P_G6OlXBe,X6[S/g ;mG`X?j?k&S-R`~f1P<X6vZTYS@NtyV|$9Ja5_o');
define('NONCE_KEY',        '|x# _E9Mf!PJV!<aT5gt#AV9{Xl$iM7&b2Z&*|!#:Xip4W~].i(/(*/.Q#,~)GI)');
define('AUTH_SALT',        's]~Dn[wN$1+fvQ^?^z:/sW&|)2Rlu2bkV+}<}0D)p^nF._8Z<Rx_ax)s=IJLn)4t');
define('SECURE_AUTH_SALT', 'Y{|$k%7]pDcLN]*2_SL4L`Y`C!@C|e eakoFNf+/uOrFLfRO,hQ7CsC/9f:Kzh_L');
define('LOGGED_IN_SALT',   '-rizhV& 7(]X9M=qAxKI7qG+I`m8+*s+z>X91Nw+x,E/e&-M!&Iyzc-ZYkd;Ozul');
define('NONCE_SALT',       '4^V#/&.8nF])|^m?+q+1rEGDc[?c:Ct@zCi<D8K8_7+uHv~kNm6w28z4|Wq+oY;L');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
