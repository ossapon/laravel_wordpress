# README #

Test projects in Laravel Login and Registration page and Worpress theme.

Appach version 2.4

- include path to project .conf file in httpd.conf

- sample :
	Include "[path-to-folder]/myconf.conf"

	Conf file for Laravel in laravel_reg_login folder
	
- conf: myconf.conf

- port: 8100

- host: localhost

	Conf file for Wordpress theme in wordpress folder

- conf: myconf.conf

- port: 8200

- host: localhost


Database configuration

Laravel DB:

- DBname: laravel

- DBuser: root

- DBpass: 111111
	
	
Wordpress DB:

- DBname: worpress

- DBuser: root

- DBpass: 111111

Wordpress Login/Pass:

- wp_login: admin
- wp_password: admin

Created by Alex Sapon

UNIT Factory student

o.s.sapon@gmail.com
